---
title : "Reporting a phishing site"
date : 2018-08-24T16:01:23+08:00
description : "Reporting a phishing site"
draft : false

---

#### __Update__: All these websites were taken down, you can view the archived versions on archive.org

My mom received this message on WhatsApp, someone forwaded it to her. The message was written in hindi and basically said that users can earn Rs. 60000/- sitting at home and doing nothing and asked them to fill a form on this page -> __www.yuva-rozgaar-yojna-register [dot] ga/__
<!--more-->
## About the Site

That site was hosted by Google's Blogspot service and he used Let's Encrypt to generate certificate for the domain. He didn't even buy the domain, anyone can get .ga domain for free.

{{< figure alt="1st Page from the site" src="/img/reporting-a-phishing-site/1.png" title="1st Page" >}}

I archived this page, [Internet Archive - Site 1](https://web.archive.org/web/20180825132703/https://www.yuva-rozgaar-yojna-register.ga/). This page wants you to give your Name, Contact number and your location (City). He doesn't actually wants that data, nothing is sent anywhere instead it takes you to this page -> __invite-yuva-rozgaar.blogspot [dot] com/__

One more thing, he is using India's Prime Minister's Photo everywhere, so as to gain people's trust. This is how the second page looks.

{{< figure alt="2nd Page from the site" src="/img/reporting-a-phishing-site/2.png" title="2nd Page" >}}

Now he asks users to share this to their WhatsApp contact in order to increase their salary or whatsoever. People actually beleive it and they share it with their contacts. That brings us to the third page which asks users to download an app -> __confirm-order.blogspot [dot] com/__. The app is NewsDog on GPlay and the reward is 50Rs for downloading and installing it.

{{< figure alt="3rd Page from the site" src="/img/reporting-a-phishing-site/3.png" title="3rd Page" >}}

[Internet Archive - Site 2](https://web.archive.org/web/20180825133526/https://invite-yuva-rozgaar.blogspot.com/)

[Internet Archive - Site 3](https://web.archive.org/web/20180825133713/https://confirm-order.blogspot.com/)

[Internet Archive - NewsDog Page](https://web.archive.org/web/20180825133823/http://ndog.co/h/hoynhg/?t=1532797610467)

### What did he get from this?

If you look closely you'll notice a banner at the end of every site and that takes us to this blog -> __buildmebest [dot] com/__

He did this for generating a few clicks to his blog where he is probably using Adsense. I have reported this to google and I hope they take it down and suspend his account.

### Prevent Phishing

Here are a few articles that you must read to prevent and detect sites like these. 

- [How to Prevent Phishing](https://www.wikihow.com/Prevent-Phishing)
- [How to Spot an Email Hoax or Phishing Scam](https://www.wikihow.com/Spot-an-Email-Hoax-or-Phishing-Scam)
- [How to Report Phishing](https://www.wikihow.com/Report-Phishing)
