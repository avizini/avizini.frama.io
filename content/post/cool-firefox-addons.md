---
title: "Cool Firefox Addons"
date: 2018-12-02T22:09:04+05:30
draft: false
description: "Reviewing some cool Firefox Addons"

---

This is the first post of my new series Firefox Addons where I review some cool firefox addons. Nothing is sponsored and everything that I write about those addons is actually written by me. I will review the addons that I use.
<!--more-->

#### __Note__: *Previously I was making different posts for new addons but that way every post was around 100 words only so I've decided to combine all of them in one post. This page will be completed later.*

## Dark Background and Light Text
---

*Dark Background and Light Text* is an addon that allows you to change background/foreground colors of the websites you visit. Current version: 0.6.10

{{< figure alt="Dashboard image" src="/img/firefox-addons/dark-background-and-light-text/dashboard.png" title="Dashboard - Dark Background and Light Text" >}}

Those are the options that you can play with, currently my color scheme is the same as this blog's -> [Solarized dark](https://github.com/samme/base16-styles/blob/master/css/base16-solarized-dark.css). You can also add rules for specific domains, for example you can disable the adodn for example.org or use invert option for example.com. Below is a gif showing how it works by using it on github.

{{< figure alt="Test run on github" src="/img/firefox-addons/dark-background-and-light-text/test-run.gif" title="Test Run" >}}

Isn't that cool? There are a lot of addons that allow you to do the same but this is the simplest one that I found, also this is open source and hosted on [GitHub](///github.com/m-khvoinitsky/dark-background-light-text-extension).

## Decentraleyes
---

Decentraleyes blocks resources from CDNs and manually injects it into websites, all this is done locally and instantly. The dashboard is dead simple, you don't have to configure anything for it to work.

{{< figure alt="Config Image" src="/img/firefox-addons/decentraleyes/config.png" >}}

It is open source and hosted at [GitLab - Self Hosted](https://git.synz.io/Synzvato/decentraleyes).