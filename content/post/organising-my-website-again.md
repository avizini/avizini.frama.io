---
title: "Organising my website again"
date: 2018-12-02T13:58:21+05:30
description: "Reorganising my website"
draft: false

---

I switched to [this color scheme](https://github.com/samme/base16-styles/blob/master/css/base16-solarized-dark.css) recently and I am going to stick with this for some time. Previously this page was hosted on avizini.me/blog but now I've moved this to avizini.me and will keep my projects at avizini.me/projects.
<!--more-->

Currently there are only two projects that I've worked on and both of them are Portfolio templates. I've named this repository as blog because whenever I rename it to avizini.github.io, GitHub assumes that this is the source for a Jekyll blog and automatically publishes from master branch. I don't know why it is doing this, I don't even get an option to change that manually.

**Update**: Current color scheme is Materia (same repository parent folder) and not solarized dark. 