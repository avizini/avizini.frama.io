---
title: "About"

---
This blog is hosted by Framasoft.

### Credits

- Hugo Theme - Base16
- Hugo - Static Site Generator
- Hosted by GitHub
- All images used are free to use unless mentioned & the source is always mentioned below the image.

[Source](https://framagit.org/avizini/avizini.frama.io)