---
title: "Contact"

---

### Instant Messengers

- Telegram (@avizini)
- [Keybase](///keybase.io/avizini) (@avizini)
- XMPP (@avizini on chat.disroot.org/ riseup.net)

### Email

- hello [at] avizini [dot] me
- avizini [at] disroot [dot] org

### Social Media

- Mastodon (@avizini on social.weho.st)

### PGP Key

You might get a link to another key from someone else, I do have multiple keys. You should delete those and download my primary key from here. Currently I have access to only 2 keys.

__Fingerprint__: *1376 2721 A8BA 4133 63BD 8026 1BE8 894E 3D7C 61A1*

- [avizini.me/pub.asc](/pub.asc)
- [Keybase](https://keybase.io/avizini/pgp_keys.asc?fingerprint=13762721a8ba413363bd80261be8894e3d7c61a1)
- [Riseup Share](https://share.riseup.net/#OohTynArVpwNuWnlHIzSHg)
- [Private Bin - Disroot](https://bin.disroot.org/?861239a14e22944c#OvG0XZo25ul0Ci8E5vH0AMTsSiEZDAyzvW4zgASTzTs=)
- [Zerobin dot net](https://zerobin.net/?ee3ac85986eff83d#sjJdNNSBu23R84nDYfKivAspAQ3Ec+7wJB7sSqaPyi8=)